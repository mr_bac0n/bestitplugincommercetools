<?php

namespace BestItPluginCommercetools;

use Shopware\Components\Plugin;

class BestItPluginCommercetools extends Plugin
{
    /** Hauptklasse des Plugins
     *
     *  Lifecycle-Methoden wie install, uninstall, activate...
     *
     *  Events/Controller können hier subcribed werden! (Oder eigenen Service erstellen der andere Services nutzt (Subscribers))
     *
     *
    */

    /**
     * onPreDispatch wurde in ein Subscriber 'custom/plugins/BestItPluginCommercetools/Subscribers/BestItSubscriber.php" ausgelagert!
     */
/*
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch' => 'onPreDispatch'
        ];
    }

    public function onPreDispatch(\Enlight_Event_EventArgs $args)
    {
       /** @var \Shopware_Controllers_Frontend_RoutingDemonstration $controller */
/*
        $controller = $args->getSubject();

        $controller->View()->addTemplateDir(__DIR__.'/Resources/views');
    }
*/


}

